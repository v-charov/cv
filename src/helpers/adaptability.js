export default {
  values: {
    mobileL: 425,
    tablet: 768,
    laptop: 1024,
    laptopL: 1440,
  },
  get isMobile() {
    return window.innerWidth < 425;
  },
  get isMobileL() {
    return window.innerWidth >= 425 && window.innerWidth < 768;
  },
  get isTablet() {
    return window.innerWidth >= 768 && window.innerWidth < 1024;
  },
  get isLaptop() {
    return window.innerWidth >= 1024 && window.innerWidth < 1440;
  },
  get isLaptopL() {
    return window.innerWidth >= 1440;
  },
};
