export default (el) => {
  const rect = el.getBoundingClientRect();

  return {
    width: rect.width,
    height: rect.height,
    top: rect.top + window.scrollY,
    left: rect.left + window.scrollX,
  };
};
