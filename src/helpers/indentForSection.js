import adaptability from '@/helpers/adaptability';

export default () => {
  let offset = 0;

  if (adaptability.isMobile || adaptability.isMobileL) offset -= 61;
  if (adaptability.isTablet) offset -= 79;
  if (adaptability.isLaptop || adaptability.isLaptopL) offset -= 9;

  return offset;
};
