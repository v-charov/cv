export default function () {
  const indent = window.innerWidth - document.documentElement.clientWidth;
  document.body.style = `padding-right: ${indent}px; overflow: hidden`;
}
