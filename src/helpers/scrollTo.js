import getOffset from '@/helpers/getOffset';

export default (elementId, offset = 0) => {
  const element = document.getElementById(elementId);
  let { top } = getOffset(element);

  top += offset;

  window.scrollTo({ top, behavior: 'smooth' });
};
