import i18n from '@/i18n';
import axios from 'axios';

const Translation = {
  loadLocaleFile(locale) {
    return import(`@/lang/${locale}.js`);
  },

  isLocaleSupported(locale) {
    return Translation.supportedLocales.includes(locale);
  },

  setI18nLocaleInServices(locale) {
    Translation.currentLocale = locale;
    axios.defaults.headers.common['Accept-Language'] = locale;
    document.querySelector('html').setAttribute('lang', locale);

    return locale;
  },

  changeLocale(locale) {
    if (!Translation.isLocaleSupported(locale)) return Promise.reject(new Error('Locale not supported'));
    if (i18n.locale === locale) return Promise.resolve(locale);

    return Translation.loadLocaleFile(locale).then((message) => {
      i18n.setLocaleMessage(locale, message.default || message);

      return Translation.setI18nLocaleInServices(locale);
    });
  },

  routeMiddleware(to, from, next) {
    const { locale } = to.params;
    if (!Translation.isLocaleSupported(locale)) return next(Translation.defaultLocale);

    return Translation.changeLocale(locale).then(() => next());
  },

  i18nRoute(to) {
    return {
      ...to,
      params: { locale: this.currentLocale, ...to.params },
    };
  },

  get defaultLocale() {
    return process.env.VUE_APP_I18N_LOCALE;
  },

  get supportedLocales() {
    return process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(',');
  },

  get currentLocale() {
    return i18n.locale;
  },

  set currentLocale(locale) {
    i18n.locale = locale;
  },

  $t(key) {
    return i18n.t(key);
  },
};

export default Translation;
