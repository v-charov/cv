import layouts from './en/layouts';
import homePage from './en/home-page';

export default {
  ...layouts,
  ...homePage,
};
