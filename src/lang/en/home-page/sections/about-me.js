export default {
  aboutMe: {
    content: {
      personalQualities: [
        `analytical mindset, structural thinking, the ability to quickly navigate the situation and
         make decisions independently`,
        `the ability to break down functionality into components and combine them into others,
         more complex components`,
        'ability to evaluate your product in terms of UI/UX',
        `the ability to get into the project, to understand who the end user is, why is this or
         that feature/opportunity being implemented and what is its business value`,
        `skills of working with large amounts of information, attention to detail, responsibility,
         purposefulness, persistence and aspiration for professional growth`,
      ],
    },
    resumeBtn: 'Experience',
    resume: {
      title: 'Experience',
      desc: [
        {
          company: 'MHP is the largest producer and exporter of chicken meat in Ukraine',
          period: 'December 2022 – February 2024',
          position: 'Senior Front End developer',
          desc: `MHP specializes in chicken production and grain cultivation,
            as well as other agricultural activities (production of meat and
            sausage products and ready-to-eat meat products). My responsibilities included:`,
          duties: [
            'customization of Front-end part build using VueJS 3, VueUse, Vue auto routing, Pinia, TailwindCSS, ViteJS, EsLint',
            'building project architecture, implementing business logic, developing snippets, components, and maintainers',
            'support, refinement and optimization of the old code base',
            'management of the development team, Code Review, mentoring and training of team members',
          ],
        }, {
          company: 'ITENDO – an international software research and development company',
          period: 'June 2022 - October 2022',
          position: 'Senior Front End developer',
          desc: `ITENDO is an international software research and development company. It is engaged in the support
          and improvement of the international HandlarFinans project. My responsibilities included:`,
          duties: [
            'configuring Front-end part build using VueJS 3, VueRouter, Pinia, TailwindCSS, VIteJS, EsLint, Prettier',
            'building project architecture, implementing business logic, developing snippets, components, and maintainers',
            'support, refinement and optimization of the old code base',
            'management of the development team, Code Review, mentoring and training of team members',
          ],
        }, {
          company: 'DANVEL - international brokerage, asset management, investments',
          period: 'April 2022 - May 2022',
          position: 'Senior Front End developer',
          desc: `DANVEL is an international brokerage portal providing
          asset management and investment. It has licenses in Cyprus (CySec),
          Republic of Mauritius (FSC) and Estonia (FIU). My responsibilities included:`,
          duties: [
            `customizing the Front-end part build using Nuxt 2, Webpack, EsLint, CommitLint,
            StyleLint`,
            `building project architecture, implementation of business logic, development of snippets,
             components, helper`,
            'front-end development from scratch',
            'writing wrapper components for ChartJS and V-Calendar libraries',
            'connecting, configuring Axios module and integrating business logic with API',
            'management of the development team, Code Review, mentoring and training of team members',
          ],
        }, {
          company: 'TravelAsk is a travel search engine',
          period: 'July 2018 - February 2022',
          position: 'Front-end developer <i></i> Team lead Front-end developer',
          desc: `TravelAsk is the largest travel site on the Runet, with an audience of over
          7 million unique users per month. unique users per month. My duties included:`,
          duties: [
            `management of the development team, hiring and adaptation of new employees,
             quality control, Code Review, mentoring and training of team members`,
            'building project architecture, implementing business logic, writing snippets, components, helper',
            'support and development of new functionality on the portal',
            'development of new promotional projects (Front-end parts) from scratch',
            `development of promotional materials for new projects (animated branding and banners),
             as well as setting up the assembly of these materials using Gulp`,
            'configuring the Front-end part build using Webpack',
            'creation of functional APIs (joint work with Back-end developers)',
            'seo optimization pages (PageSpeed), the removal of commercial directions in separate entry points',
            'introduction of new technologies and methodologies',
          ],
        }, {
          company: 'LLC Marketing Group Kompleto',
          period: 'March 2018 - May 2018',
          position: 'Junior Front-end developer',
          desc: 'My duties included:',
          duties: [
            `adaptive and cross-browser layout of new pages and projects using the methodology BEM in
             compliance with W3C standards`,
            'Front-end development related to these pages (calculators, lightboxes, sliders, etc.)',
            'making changes to finished projects, finalizing projects',
          ],
        },
      ],
    },
  },
};
