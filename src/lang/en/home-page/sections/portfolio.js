export default {
  portfolio: {
    generalDesc: 'You can read about the project or go to the site by following this link',
    list: {
      mhp: {
        title: 'MHP',
        stack: ['Vue 3', 'TailwindCSS', 'ViteJS', '...'],
        link: 'https://mhp.com.ua/uk/glorytoUkraine',
        screenshots: [
          {
            name: 'mhp-home',
            desc: 'Home page',
          }, {
            name: 'mhp-equipments',
            desc: 'Equipment list',
          }, {
            name: 'mph-equipments-662',
            desc: 'Equipment',
          }, {
            name: 'mhp-work-instructions',
            desc: 'Builder for creating instructions for equipment',
          }, {
            name: 'mhp-equipments-refurbishable-units-units',
            desc: 'Work with equipment',
          }, {
            name: 'mhp-tasks-list',
            desc: 'List of tasks',
          }, {
            name: 'mhp-tasks-filter',
            desc: 'Task filtering logic',
          }, {
            name: 'mhp-tasks-create',
            desc: 'Logic of task creation',
          }, {
            name: 'mhp-warehouse',
            desc: 'Compositions and working with them',
          }, {
            name: 'mhp-warehouse-purchase-requests',
            desc: 'Work with warehouse requisitions',
          }, {
            name: 'mhp-warehouse-catalog',
            desc: 'Logic of work with spare parts (description, editing, grouping analog/original)',
          }, {
            name: 'mhp-warehouse-write-offs-main-assets',
            desc: 'The logic of writing off spare parts',
          }, {
            name: 'mhp-analytics-equipments-availability',
            desc: 'Work center analytics',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              `MHP is the largest producer and exporter of chicken meat in Ukraine. The company specializes in chicken
              production and grain cultivation, as well as other agricultural activities (production of meat and sausage
              products and ready-to-eat meat products).`,
            ],
          },
          'my-role': {
            title: 'My role',
            list: [
              'customization of Front-end part build using VueJS 3, VueUse, Vue auto routing, Pinia, TailwindCSS, ViteJS, EsLint',
              'building project architecture, implementation of business logic, development of snippets, components, helpers',
              'support, refinement and optimization of the old code base',
              'management of the development team, Code Review, mentoring and training of team members',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'Vue 3',
              'VueUse',
              'Pinia',
              'SCSS',
              'TailwindCSS',
              'Fetch',
              'NaivUI',
              'ViteJS',
              'yarn',
              'Git',
            ],
          },
        },
      },
      handlarfinans: {
        title: 'Handlarfinans',
        stack: ['Vue 3', 'TailwindCSS', 'ViteJS', '...'],
        link: 'https://handlarfinans.se/',
        screenshots: [
          {
            name: 'handlarfinans-home',
            desc: 'Home page',
          }, {
            name: 'handlarfinans-purchases',
            desc: 'Purchases',
          }, {
            name: 'handlarfinans-marketplace',
            desc: 'Marketplace',
          }, {
            name: 'handlarfinans-invoices',
            desc: 'Invoices',
          }, {
            name: 'handlarfinans-contracts',
            desc: 'Contracts',
          }, {
            name: 'handlarfinans-settings',
            desc: 'Settings',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              `Handlarfinans is an international portal that provides financing and buying opportunities for dealers 
              vehicles across Northern Europe to help more entrepreneurs, fully 
              reach their full potential.`,
            ],
          },
          'my-role': {
            title: 'My role',
            list: [
              `Front-end part build setup using VueJS 3, VueRouter, Pinia, TailwindCSS, VIteJS, 
              EsLint, Prettier`,
              `building project architecture, implementation of business logic, development of snippets, components, 
              and maintainers`,
              'support, refinement and optimization of the old code base',
              'management of the development team, Code Review, mentoring and training of team members',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'Blade',
              'Vue 2',
              'Vue 3',
              'Pinia',
              'Vuex',
              'Pug',
              'SCSS',
              'TailwindCSS',
              'Axios',
              'VeeValidate',
              'ViteJS',
              'Webpack',
              'NPM',
              'Git',
            ],
          },
        },
      },
      danvel: {
        title: 'Danvel',
        stack: ['NuxtJS', 'Vue 2', 'composition api', '...'],
        link: 'https://danvel.com/',
        screenshots: [
          {
            name: 'danvel-home',
            desc: 'Home page',
          }, {
            name: 'danvel-my-account',
            desc: 'My account',
          }, {
            name: 'danvel-suggestions',
            desc: 'Suggestions',
          }, {
            name: 'danvel-structure',
            desc: 'Structure',
          }, {
            name: 'danvel-my-finances',
            desc: 'My finances',
          }, {
            name: 'danvel-reports',
            desc: 'Reports',
          }, {
            name: 'danvel-profile',
            desc: 'Profile',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'DANVEL is an international brokerage portal providing asset management and investment opportunities.',

              'It has licenses in Cyprus (CySec), the Republic of Mauritius (FSC) and Estonia (FIU).',
            ],
          },
          'my-role': {
            title: 'My role',
            list: [
              `Front-end part build configuration using Nuxt 2, Webpack, EsLint, CommitLint,
            StyleLint, Pinia integration to store application state`,
              `building project architecture, implementation of business logic, development of snippets,
             components, helper`,
              'front-end development from scratch',
              'writing wrapper components for ChartJS and V-Calendar libraries',
              'connecting, configuring Axios module and integrating business logic with API',
              'management of the development team, Code Review, mentoring and training of team members',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'NuxtJS 2',
              'Composition API',
              'Vue 2',
              'Pinia',
              'Pug',
              'SASS',
              'Axios',
              'ChartJS',
              'V-Calendar',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      travelask: {
        title: 'TravelAsk',
        stack: ['Ruby on Rails', 'Liquid', 'SASS', 'ES6', '...'],
        link: 'https://travelask.ru',
        screenshots: [
          {
            name: 'travelask-home',
            desc: 'Home Page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              `TravelAsk is a search engine for the world of travel. Searches and compares the best deals on tours,
               hotels and airline tickets. Inspires new trips and entertains with content.`,

              `The company was founded in 2015 by Alexei Bobrov, and at the time the service worked
               as a question-and-answer service for travelers. People were interested  from Izhevsk to Baku,
               whether the airport in Marseille closes at night, and so on.`,

              `In 2017, the project took a powerful step forward. It stopped being a service
               in question-and-answer service, and combined everything a tourist needs when traveling.
               These are guides, tours, airline tickets, hotels, railways, buses, and excursions.
               And also launched magazine and started writing news.`,

              `Now TravelAsk is a guide to the world of travel. It helps you find the best deals in the field of travel.
               It has an audience of more than 7 million unique users per month.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'support and development of new functionality on the portal',
              `layout of new blocks and landings according to the BEM methodology, using the principles of Mobile First
               and Progressive Enhancement`,
              'building project architecture, implementing business logic, writing snippets, components, and maintainers',
              `development of new verticals, such as: airfare, hotels, insurance, car rentals, excursions,
               buses and rail transport`,
              'developing promo projects from scratch on pure JS using ES6 classes',
              `development of promotional materials for new promotional projects (animated branding and banners),
               As well as setting up the assembly of these materials using Gulp`,
              'creation of functional APIs (joint work with Back-end developers)',
              'seo optimization pages (PageSpeed), the removal of commercial directions in separate entry points',
              'configuring the Front-end part build using Webpack',
              `management of the development team, quality control of the work performed, Code Review, mentoring
               and training of team members`,
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'HTML',
              'Pug',
              'Slim',
              'Handlebars',
              'Liquid',
              'BEM',
              'CSS',
              'SASS',
              'SCSS',
              'ES6',
              'ES7',
              'ES8',
              'jQuery',
              'Axios',
              'Leaflet',
              '_lodash',
              'JSON',
              'Vue',
              'Webpack',
              'Gulp',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      yourchoice: {
        title: 'Yourchoice',
        stack: ['Pug', 'SASS', 'ES6', '...'],
        link: 'https://yourchoice.travelask.ru',
        screenshots: [
          {
            name: 'yourchoice-home',
            desc: 'Home Page',
          }, {
            name: 'yourchoice-inner',
            desc: 'Certificate draw page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'This is a promotional project for BAT Russia. YourChoice is about habits of purity.',

              `As part of this promotional project, two landings were developed: yourchoice.travelask.ru
               and spec.yourchoice.ru, these are essentially funnels for generating leads.`,

              `The project itself provides information on rules for visiting COVID-related countries
               and rules for those who smoke.`,

              `The project has an adaptive and rubberized layout, the markup is implemented in pug,
               styles - sass, the principles of Mobile First and Progressive Enhancement were used,
               the logic is written in pure JavaScript using ES6 classes. Project build was written with webpack version 5.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'configuring the Front-end part build using Webpack',
              'project architecture',
              'implementing business logic from scratch in pure JS using ES6 classes',
              'management of the development team, quality control of work performed, Code Review',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'HTML',
              'Pug',
              'BEM',
              'SASS',
              'ES6',
              'ES7',
              'ES8',
              '_lodash',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      gurmegid: {
        title: 'Gourmet guide',
        stack: ['Ruby on Rails', 'Slim', 'SASS', 'ES6', '...'],
        link: 'https://gurmegid.travelask.ru/',
        screenshots: [
          {
            name: 'gurmegid-home',
            desc: 'Home Page',
          }, {
            name: 'gurmegid-game',
            desc: 'Online game',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'The promo project for Bifiform is a probiotic complex for the whole family.',

              `As a part of this promotional project, developed a webpage with entertainment content
               and an online game with an opportunity to win a trip for two to one of the resort cities.`,

              'The project itself provides a funnel for the formation of leads.',

              `The project has an adaptive and rubberized layout, the markup is implemented in slim,
               styles - sass, the principles of Mobile First and Progressive Enhancement were used,
               the logic is written in pure JavaScript using ES6 classes. Online-game is implemented in Vue.
               Assembly of the project is implemented on webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'configuring the Front-end part build using Webpack',
              'project architecture',
              'implementing business logic from scratch in pure JS using ES6 classes',
              'creation of functional APIs (joint work with Back-end developers)',
              'management of the development team, quality control of work performed, Code Review',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'HTML',
              'Pug',
              'Slim',
              'BEM',
              'SASS',
              'ES6',
              'ES7',
              'ES8',
              'Vue',
              'Axios',
              '_lodash',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      lenovo: {
        title: 'Lenovo',
        stack: ['Ruby on Rails', 'Liquid', 'SASS', 'ES6', '...'],
        link: 'https://lenovo.travelask.ru/',
        screenshots: [
          {
            name: 'lenovo-home',
            desc: 'Home Page',
          }, {
            name: 'lenovo-test',
            desc: 'Test page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'A promo project for Lenovo and their new product, the Lenovo Yoga Slim 7i Fabric.',

              `As part of this promotional project, a webpage with entertainment content was developed,
               essentially a guide to the world's most impressive designer hotels, with the opportunity
               to win a trip to one of these hotels.`,

              `The project itself provides a lead generation funnel and has a small questionnaire
               with an opportunity to choose the most impressive designer hotel in the world.`,

              `The project has an adaptive and rubberized layout, the markup is implemented in slim,
               styles - sass, the principles of Mobile First and Progressive Enhancement were used,
               the logic is written in pure JavaScript using ES6 classes. Project build was written with webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'configuring the Front-end part build using Webpack',
              'project architecture',
              'implementing business logic from scratch in pure JS using ES6 classes',
              'creation of functional APIs (joint work with Back-end developers)',
              'management of the development team, quality control of work performed, Code Review',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'HTML',
              'Pug',
              'Slim',
              'BEM',
              'SASS',
              'ES6',
              'ES7',
              'ES8',
              'Axios',
              '_lodash',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      bifiform: {
        title: 'Bifiform',
        stack: ['Ruby on Rails', 'Slim', 'SASS', 'ES6', '...'],
        link: 'https://bifi.travelask.ru//',
        screenshots: [
          {
            name: 'bifiform-home',
            desc: 'Home Page',
          }, {
            name: 'bifiform-comics',
            desc: 'Interactive comics page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'The promo project for Bifiform is a probiotic complex for the whole family.',

              `As part of this promotional project, a landing page was developed with entertaining content
               and opportunity to pass a comic book and win a gourmet trip for two.`,

              `The project itself is a lead generation funnel and an interactive comic book
               (a step-by-step tour with the story of the journey).`,

              `The project has an adaptive and rubberized layout, the markup is implemented in slim,
               styles - sass, the principles of Mobile First and Progressive Enhancement were used,
               the logic is written in pure JavaScript using ES6 classes. Interactive comic book tour
               is implemented with the help of Bootstrap Tour plugin. The project is built on the webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'configuring the Front-end part build using Webpack',
              'project architecture',
              'implementing business logic from scratch in pure JS using ES6 classes',
              'creation of functional APIs (joint work with Back-end developers)',
              'management of the development team, quality control of work performed, Code Review',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'HTML',
              'Pug',
              'Slim',
              'BEM',
              'SASS',
              'ES6',
              'ES7',
              'ES8',
              'Axios',
              '_lodash',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      altaprofil: {
        title: 'Alta-Profile',
        stack: ['PHP', 'SASS', 'ES5', '...'],
        link: 'https://www.alta-profil.ru/',
        screenshots: [
          {
            name: 'alta-profil-home',
            desc: 'Home Page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              `Alta-Profile is one of Europe's largest multi-profile production PVC cladding products
               production complex.`,

              `The old site was completely redesigned. An order management system was implemented for manufacturers
               and dealers, a complete product catalog and the launch of 20 landing product pages.`,

              `The project has an adaptive and rubberized layout, the layout is implemented in php,
               styles - scss, the logic is written jquery. The project is built on webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'layout of new blocks according to BEM methodology, using Mobile First and Progressive Enhancement principles',
              'support and development of new functionality on the portal',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'PHP',
              'CSS',
              'SCSS',
              'ES5',
              'Jquery',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      torex: {
        title: 'Torex',
        stack: ['PHP', 'SASS', 'ES5', '...'],
        link: 'https://torex.ru/',
        screenshots: [
          {
            name: 'torex-home',
            desc: 'Home Page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'Torex is a large steel door factory with an extensive network of dealers and sub-dealers.',

              `The project has an adaptive and rubberized layout, the layout is implemented in php,
               styles - scss, the logic is written jquery. Assembly of the project is implemented
               on webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'layout of new blocks according to BEM methodology, using Mobile First and Progressive Enhancement principles',
              'support and development of new functionality on the portal',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'PHP',
              'CSS',
              'SCSS',
              'ES5',
              'Jquery',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
      queenmarine: {
        title: 'Queen Marine',
        stack: ['PHP', 'SASS', 'ES5', '...'],
        link: 'https://queenmarine.ru/',
        screenshots: [
          {
            name: 'queenmarine-home',
            desc: 'Home Page',
          },
        ],
        content: {
          'about-project': {
            title: 'About project',
            desc: [
              'Aquileia has been developing and producing cosmetics since 2002.',

              `In 2015, the company launched an innovative series of cosmetics Queen Marine based
               on Gulf Stream seawater and active ingredients of marine origin.`,

              `The project has an adaptive and rubberized layout, the layout is implemented in php,
               styles - scss, the logic is written jquery. Assembly of the project is implemented
               on webpack version 4.`,
            ],
          },
          'my-role': {
            title: 'My Role',
            list: [
              'layout of new blocks according to BEM methodology, using Mobile First and Progressive Enhancement principles',
              'support and development of new functionality on the portal',
            ],
          },
          'technical-description': {
            title: 'Technical description',
            desc: ['The technologies I worked with on this project.'],
            list: [
              'PHP',
              'CSS',
              'SCSS',
              'ES5',
              'Jquery',
              'JSON',
              'Webpack',
              'Yarn',
              'Git',
            ],
          },
        },
      },
    },
    detail: 'View',
    view: 'Go to',
  },
};
