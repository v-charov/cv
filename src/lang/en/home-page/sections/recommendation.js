export default {
  recommendation: {
    list: [
      {
        company: 'ITendo',
        image: 'ITendo.jpg',
        url: 'https://itendo.io',
        person: 'Oleksii Kulinich',
        position: 'Founder & CEO',
        date: '15.10.2022',
        text: [
          'Vitaly Сharov worked at ITendo as a front-end architect.',
          `Vitaly passed the interview the first time and immediately joined our established team,
          quickly and easily, which shows him as a team player. Throughout our work, Vitaly has established
          himself as a web development professional with an impressive skill set and extensive experience.
          Thanks to his analytical mindset and structural thinking, he writes efficient code well.`,
          `I can also recognize his sense of UI/UX and ability to evaluate a product from a design and user
          experience perspective. Vitaly is not just a developer, he dives deep into the project and understands
          the end user and the business value of the functionality.`,
          `Vitaly impresses with his detail, persistence, and constant desire for professional growth.
          He treats his tasks with a high degree of responsibility.`,
          `From personal qualities, calmness, responsiveness, self-control, ability to get along with other team members,
          communication skills and willingness to help, always open to dialog, quickly copes with complex tasks,
          and ability to admit his mistakes.`,
          'Vitaly was very comfortable, pleasant, and easy to work with.',
        ],
      }, {
        company: 'travelask',
        image: 'travelask.svg',
        url: 'https://travelask.ru',
        person: 'Alexey Bobrov',
        position: 'Company head',
        date: '12.01.2022',
        text: [
          'Vitaly Charov worked at TravelAsk as a fornt-end developer.',
          `During his work, Vitaly proved to be a competent and responsible employee, He independently
           and successfully solved the tasks assigned to him. Vitaly has made a huge contribution
           in project development, participated in development of new verticals: air tickets, hotels, insurance,
           car rent, tours, buses and rail transport, maintained old parts of the project, did code refactoring,
           done prior to joining the company. In 2020 was promoted to team lead And led a team
           of 4 fornt-end developers.`,
          `He is neat and thoughtful, and is responsible for completing tasks. Responsive,
           always ready to help colleagues. Calm, well-balanced. High level of JS, Vue.js framework.
           Handles complex and non-trivial tasks. Ready to work overtime in force majeure circumstances.`,
          `Vitaly's level of professionalism exceeds the needs of our company, so he decided to look
           for a place to more fully realize all of his abilities and new areas of growth.`,
        ],
      }, {
        company: 'completo',
        image: 'completo.jpg',
        url: 'https://www.completo.ru',
        person: 'Nadezhda Tsyvinskaya',
        position: 'Head of Development',
        date: '14.05.2018',
        text: [
          'Vitaly Charov worked in the company "Kompleto" under my supervision as a junior front-end developer.',
          `His responsibilities included layout layout of the company's projects from scratch,
           revision of of the company's existing projects as part of technical support.`,
          `During his work in the company Vitaly showed his good knowledge of HTML5/CSS3 as well
           as knowledge of the basics of php and js.`,
          `Vitaly established himself as an inquisitive and eager to develop employee, who easily works
           in a team and is able to understand other people's code independently.`,
          `From personal qualities it is worth noting, the ability to work remotely - independently plan
           to plan their time, to interact with the team in a timely manner.`,
          `Therefore, I believe that he may qualify for the position of junior front-end developer
           with the prospect of further development in another company.`,
        ],
      },
    ],
  },
};
