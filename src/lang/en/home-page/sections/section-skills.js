export default {
  skills: {
    desc: [
      `I know HTML5, CSS3, JavaScript, understand frameworks and libraries, understand what
       is "under the hood" on the server side, which is implemented by back-end developers
       (Node.js, PHP, Ruby\u00a0on\u00a0Rails).`,
      `I have skills in adaptive, cross-browser, valid layout in compliance with W3C standards.
       I write semantic code according to BEM methodology, using the principles of Mobile First and
       Progressive Enhancement. I work with templating tools such as Slim, Pug, Twig, Handlebars,
       Liquid and SASS, SCSS, LESS preprocessors.`,
      `I can write high-performance asynchronous code in pure JavaScript using ES6 classes.
       I have knowledge and experience with popular frameworks and libraries, such as NuxtJS, VueJS,
       Nuxt Composition API, Vuex, Pinia, Vue Router, jQuery. I know how to work with DOM, API, SVG objects,
       AJAX and CORS.`,
    ],
  },
};
