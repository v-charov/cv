export default {
  bar: {
    menuBtn: 'MENU',
    menu: {
      home: 'HOME',
      aboutMe: 'ABOUT ME',
      skills: 'SKILLS',
      resume: 'RESUME',
      portfolio: 'PORTFOLIO',
      recommendation: 'RECOMMENDATIONS',
    },
    socialList: {
      telegram: 'TELEGRAM',
      skype: 'SKYPE',
      whatsapp: 'WHATS APP',
      google: 'GOOGLE PLUS',
    },
  },
};
