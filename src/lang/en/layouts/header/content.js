export default {
  content: {
    title: ['Hi,', 'I am Vitaly Charov,', '<span>Front-end developer</span>'],
    desc: `Able to create a web presence from the ground up - from building project architecture,
    navigation, layout and programming to UX. Qualified in writing well-designed, testable,
    and effective code using current best practices in web development`,
    action: 'SCROLL DOWN TO DISCOVER',
  },
};
