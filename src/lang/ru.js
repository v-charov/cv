import layouts from './ru/layouts';
import homePage from './ru/home-page';

export default {
  ...layouts,
  ...homePage,
};
