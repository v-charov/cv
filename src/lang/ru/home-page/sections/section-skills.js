export default {
  skills: {
    desc: [
      `Знаю HTML5, CSS3, JavaScript, разбираюсь во фреймворках и библиотеках, понимаю,
       что находится «под капотом» на серверной стороне, которую реализовывают Back-end
       разработчики (Node.js, PHP, Ruby\u00A0on\u00A0Rails).`,
      `Имею навыки адаптивной, кросс-браузерной, валидной верстки с соблюдением стандартов W3C.
       Пишу семантический код по методологии БЭМ, используя принципы Mobile First и
       Progressive Enhancement. Работаю с шаблонизаторами такими как Slim, Pug, Twig, Handlebars,
       Liquid и препроцессорами SASS, SCSS, LESS.`,
      `Умею писать высокопроизводительный асинхронный код на pure JavaScript с использованием
       ES6 классов. Имею знание и опыт работы с популярными фреймворками и библиотеками,
       такими как NuxtJS, VueJS, Nuxt Composition API, Vuex, Pinia, Vue Router, jQuery. Умею работать с DOM, API, SVG-объектами,
       AJAX и CORS.`,
    ],
  },
};
