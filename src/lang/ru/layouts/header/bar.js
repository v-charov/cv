export default {
  bar: {
    menuBtn: 'МЕНЮ',
    menu: {
      home: 'ГЛАВНАЯ',
      aboutMe: 'ОБО МНЕ',
      skills: 'НАВЫКИ',
      resume: 'РЕЗЮМЕ',
      portfolio: 'ПОРТФОЛИО',
      recommendation: 'РЕКОМЕНДАЦИИ',
    },
    socialList: {
      telegram: 'TELEGRAM',
      skype: 'SKYPE',
      whatsapp: 'WHATS APP',
      google: 'GOOGLE PLUS',
    },
  },
};
