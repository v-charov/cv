import header from './header';
import defaultFooter from './default-footer';

export default {
  layouts: {
    ...header,
    ...defaultFooter,
  },
};
