import layouts from './ua/layouts';
import homePage from './ua/home-page';

export default {
  ...layouts,
  ...homePage,
};
