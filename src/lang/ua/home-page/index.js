import sections from './sections';

export default {
  homePage: {
    ...sections,
  },
};
