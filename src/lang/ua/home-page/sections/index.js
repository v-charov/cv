import aboutMe from './about-me';
import skills from './section-skills';
import portfolio from './portfolio';
import recommendation from './recommendation';

export default {
  sections: {
    ...aboutMe,
    ...skills,
    ...portfolio,
    ...recommendation,
  },
};
