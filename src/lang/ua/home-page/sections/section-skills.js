export default {
  skills: {
    desc: [
      `Знаю HTML5, CSS3, JavaScript, розбираюся у фреймворках та бібліотеках, розумію,
        що знаходиться під капотом на серверній стороні, яку реалізовують Back-end
        розробники (Node.js, PHP, Ruby\u00A0on\u00A0Rails).`,
      `Маю навички адаптивної, крос-браузерної, валідної верстки з дотриманням стандартів W3C.
        Пишу семантичний код з методології БЕМ, використовуючи принципи Mobile First та
        Progressive Enhancement. Працюю із шаблонізаторами такими як Slim, Pug, Twig, Handlebars,
        Liquid та препроцесорами SASS, SCSS, LESS.`,
      `Вмію писати високопродуктивний асинхронний код на pure JavaScript з використанням
        ES6 класів. Маю знання та досвід роботи з популярними фреймворками та бібліотеками,
        такі як NuxtJS, VueJS, Nuxt Composition API, Vuex, Pinia, Vue Router, jQuery. Вмію працювати з DOM, API,
        SVG-об'єктами, AJAX та CORS.`,
    ],
  },
};
