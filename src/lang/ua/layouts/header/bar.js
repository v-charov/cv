export default {
  bar: {
    menuBtn: 'МЕНЮ',
    menu: {
      home: 'ГОЛОВНА',
      aboutMe: 'ПРО МЕНЕ',
      skills: 'НАВИКИ',
      resume: 'РЕЗЮМЕ',
      portfolio: 'ПОРТФОЛІО',
      recommendation: 'РЕКОМЕНДАЦІЇ',
    },
    socialList: {
      telegram: 'TELEGRAM',
      skype: 'SKYPE',
      whatsapp: 'WHATS APP',
      google: 'GOOGLE PLUS',
    },
  },
};
