import bar from './bar';
import content from './content';

export default {
  header: {
    ...bar,
    ...content,
  },
};
