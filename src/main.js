import 'normalize.css';
import './assets/font/index.sass';

import Vue from 'vue';

import Translation from '@/lang/Translation';
import VModal from 'vue-js-modal';

import scrollTo from './helpers/scrollTo';

import App from './App';
import router from './router';
import store from './store';
import i18n from './i18n';

Vue.use(VModal, {
  injectModalsContainer: true,
});

Vue.prototype.$i18nRoute = Translation.i18nRoute.bind(Translation);
Vue.prototype.$customCursor = {};
Vue.prototype.$scrollTo = scrollTo;

Vue.config.productionTip = false;

new Vue({
  i18n,
  router,
  store,
  render: (h) => h(App),
  beforeCreate() {
    if (sessionStorage.redirect) {
      const { redirect } = sessionStorage;
      delete sessionStorage.redirect;
      this.$router.push(redirect);
    }
  },
}).$mount('#app');
