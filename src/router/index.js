import Vue from 'vue';
import VueRouter from 'vue-router';
import Translation from '@/lang/Translation';

import Locale from '@/lang/Locale';

Vue.use(VueRouter);

const load = (component) => () => import(`@/views/${component}.vue`);

const routes = [
  {
    path: '/:locale',
    component: Locale,
    props: true,
    beforeEnter: Translation.routeMiddleware,
    children: [
      {
        path: '',
        name: 'Home',
        component: load('Home'),
        meta: {
          layout: 'MainLayout',
        },
        props: true,
      },
      // {
      //   path: 'about',
      //   name: 'About',
      //   component: load('About'),
      //   meta: {
      //     layout: 'MainLayout',
      //   },
      // },
    ],
  },
  {
    path: '*',
    redirect() {
      return Translation.defaultLocale;
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
