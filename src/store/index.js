import Vue from 'vue';
import Vuex from 'vuex';

import menuState from '@/store/modules/menu-state';
import navigationLinks from './modules/navigation-links';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    menuState,
    navigationLinks,
  },
});
