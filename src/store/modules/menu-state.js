export default {
  state: {
    menuState: false,
  },
  mutations: {
    changeMenuState(state, val) {
      state.menuState = val;
    },
  },
  actions: {
    changeMenuState({ commit }, val) {
      commit('changeMenuState', val);
    },
  },
  getters: {
    menuState: (state) => state.menuState,
  },
};
