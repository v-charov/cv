import Translation from '@/lang/Translation';

export default {
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
    navLinks() {
      return [
        {
          name: 'aboutMe',
          text: Translation.$t('layouts.header.bar.menu.aboutMe'),
        }, {
          name: 'skills',
          text: Translation.$t('layouts.header.bar.menu.skills'),
        }, {
          name: 'portfolio',
          text: Translation.$t('layouts.header.bar.menu.portfolio'),
        }, {
          name: 'recommendation',
          text: Translation.$t('layouts.header.bar.menu.recommendation'),
        },
      ];
    },
  },
};
