module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? `/${ process.env.CI_PROJECT_NAME }/`
    : '/',
  runtimeCompiler: true,
  pages: {
    index: {
      entry: './src/main.js',
      template: 'public/index.html',
      title: 'Vitaly Charov',
      chunks: ['chunk-vendors', 'chunk-common', 'index'],
    },
  },

  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "~@/assets/common"',
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'ua',
      fallbackLocale: 'en',
      localeDir: 'lang',
      enableInSFC: false,
    },
  },
};
